package lesson11;

import lesson07.Pizza;
import lesson10.Inheritance.*;

import java.util.ArrayList;

public class MapApp {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<Integer>();
        ArrayList<String> strings = new ArrayList<String>();
        ArrayList<Pizza> pizzas = new ArrayList<Pizza>();
        ArrayList<Cat> cats = new ArrayList<>();
        ArrayList<Dog> dogs = new ArrayList<>();
        ArrayList<Animal> animals = new ArrayList<>();
        animals.add(new Dog());
    }
}
