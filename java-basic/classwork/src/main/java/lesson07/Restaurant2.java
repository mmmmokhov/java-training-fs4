package lesson07;

import static libs.EX.NI;

public class Restaurant2 {

    static boolean myEquals(Pizza p1, Pizza2 p2) {
        throw NI;
    }

    public static String makeName() {
        return "Marga" + "RITA".toLowerCase();
    }

    public static void main(String[] args) {
        Pizza p1 = new Pizza(makeName(), 60);
        Pizza p2 = new Pizza(makeName(), 60);
        Pizza p3 = new Pizza("Margarita", 60);
        Pizza p4 = new Pizza("Margarita", 60);
        Pizza2 p5 = new Pizza2("Margarita", 60);
        Pizza p11 = p1;

        System.out.println(p1);
        System.out.println(p4);
        System.out.println(p1 == p4);
        System.out.println(p1.equals(p5));
        System.out.println(p1.equals(p4));
        System.out.println(p1.equals("Margarita"));
    }

}
