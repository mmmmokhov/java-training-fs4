package lesson07;

public class BoxExample4 {

    public static void main(String[] args) {

        int add = Box2.add(5, 7);

        Box2.x = 33;
        Box2.print();

        Box2 b2a = new Box2(11);
        int a1 = b2a.a; // 11

        Box2 b2b = new Box2(22);
        int a2 = b2b.a; // 22

        b2a.printMe();
        b2b.printMe();

    }

}
