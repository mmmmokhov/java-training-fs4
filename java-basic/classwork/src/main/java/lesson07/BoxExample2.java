package lesson07;

public class BoxExample2 {

    public void whatever(Box2 b2) {

    }


    public static void main(String[] args) {
        System.out.println(Box2.x);
        Box2.x = 5;
        System.out.println(Box2.x); // DO THAT !!!

        Box2 box1 = new Box2(1);
        box1.x = 10; // DON'T DO THAT !!!!

        Box2 box2 = new Box2(2);

        System.out.println(box1.x);
        System.out.println(box2.x);
        System.out.println(Box2.x);
    }

}
