package lesson07;

import static libs.EX.NI;

public class Restaurant17 {

    static boolean myEquals(Pizza p1, Pizza2 p2) {
        throw NI;
    }

    public static String makeName() {
        return "Marga" + "RITA".toLowerCase();
    }

    public static void main(String[] args) {
        Pizza17 p17a = new Pizza17("Margarita", 60);
        Pizza17 p17b = new Pizza17("Margarita", 60);
        Pizza17 p17c = new Pizza17(makeName(), 60);
        System.out.println(p17a.equals(p17b)); // true
        System.out.println(p17a.equals(p17c)); // true
        System.out.println(p17a);              // Pizza17[name=Margarita, size=60]
        // getters are already implemented
        // NO SETTERS, due to FINAL
        p17a.size();
        p17a.name();
    }

}
