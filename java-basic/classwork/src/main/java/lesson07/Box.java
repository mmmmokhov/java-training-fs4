package lesson07;

import java.util.Arrays;

public class Box {

    private final int[] data;

    public Box(int[] data) {
        this.data = data;
    }

    public int[] getData() {
        return data;
    }

    public int[] getDataSafe() {
        return Arrays.copyOf(data, data.length);
    }

    @Override
    public String toString() {
        return "Box{" +
                "data=" + Arrays.toString(data) +
                '}';
    }
}
