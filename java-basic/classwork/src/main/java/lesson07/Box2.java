package lesson07;

public class Box2 {

    int a;

    static int x;

    public Box2(int a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "Box2{" +
                "a=" + a +
                '}';
    }

    public void printMe() {
        System.out.println(this);
    }

    public static void print() {
        int z = x;
        System.out.println("static print");
    }

    public static int add(int x, int y) {
        return x + y;
    }

}
