package lesson14;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CountLetters {

  record Pair<A, B>(A a, B b) {
  }

  public static Stream<Pair> makePairs(String line) {
    return IntStream.range(0, line.length())
        .mapToObj(i -> new Pair(line.charAt(i), i));
  }

  public static void main(String[] args) {
    String outcome =
        makePairs("Hello, world!")
            .collect(Collectors.groupingBy(p -> p.a))
            .entrySet().stream()
            .map(cps -> new Pair(
                    cps.getKey(),
                    cps.getValue().stream().map(x -> x.b).toList()
                )
            )
            .map(cps -> String.format("`%s`->%s", cps.a, cps.b))
            .collect(Collectors.joining(", ", "[", "]"));

    System.out.println(outcome);
  }

}
