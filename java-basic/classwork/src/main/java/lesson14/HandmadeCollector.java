package lesson14;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HandmadeCollector {

  public static void main(String[] args) {
    Set<Integer> collected = Arrays.asList(1, 2, 2, 3)
        .stream()
        .map(x -> x * 10)
        //                    data type,  accumulator type, result type
        .collect(new Collector<Integer, List<Integer>, Set<Integer>>() {
          // how to create accumulator
          @Override
          public Supplier<List<Integer>> supplier() {
            return () -> new ArrayList<>();
          }

          // how to add element to accumulator
          @Override
          public BiConsumer<List<Integer>, Integer> accumulator() {
            return (a, x) -> a.add(x);
          }

          // how to combine two lists
          @Override
          public BinaryOperator<List<Integer>> combiner() {
            return (l1, l2) -> {
              var l3 = new ArrayList<Integer>();
              l3.addAll(l1);
              l3.addAll(l2);
              return l3;
            };
          }

          // how to create result from accumulator
          @Override
          public Function<List<Integer>, Set<Integer>> finisher() {
            return l -> new HashSet<>(l);
          }

          @Override
          public Set<Characteristics> characteristics() {
            return new HashSet<>();
          }
        });
    System.out.println(collected);

  }

}
