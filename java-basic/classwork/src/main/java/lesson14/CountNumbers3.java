package lesson14;

import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.StreamHandler;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CountNumbers3 {

  static Stream<Integer> digits(Integer n) {
    return Stream
        .iterate(
            n,                              // initial element
            i -> i / 10 > 0 || i % 10 > 0,  // hasNext function
            i -> i / 10)                    // next function
        .map(i -> i % 10);
  }

  static int remapAnyToOne(String x) {
    return 1;
  }

  public static Map<String, Integer> countLetters(int min, int max) {
    return IntStream.rangeClosed(min, max)
        .boxed()
        .flatMap(CountNumbers3::digits)
        .map(c -> String.format("`%d`", c))
        .collect(Collectors.toMap(
            Function.identity(),
            CountNumbers3::remapAnyToOne,
            Integer::sum,
            TreeMap::new
        ));
  }

  public static void main(String[] args) {
    Map<String, Integer> characterIntegerMap = countLetters(173, 215);
    System.out.println(characterIntegerMap);
  }
}
