package lesson14;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsIntro {

  public static void main1(String[] args) {
    List<Integer> list = Arrays.asList(1, 2, 3);
    List<Integer> list2 = new ArrayList<>();
    list.forEach(x -> list2.add(x * 10));
    System.out.println(list);
    System.out.println(list2);
  }

  public static void main3(String[] args) {
    Random r = new Random();
    Map<Float, Map<Boolean, List<Integer>>> collect = r.ints().limit(20).boxed()
        .collect(
            Collectors.groupingBy(
                x -> Math.signum(x),
                Collectors.groupingBy(y -> y % 2 == 0)
            )
        );
    System.out.println(collect);
  }

  public static void main(String[] args) {
    Random r = new Random();
    Map<Float, Map<Boolean, Long>> collect = r.ints().limit(20).boxed()
        .collect(
            Collectors.groupingBy(
                x -> Math.signum(x),
                Collectors.groupingBy(y -> y % 2 == 0, Collectors.counting())
            )
        );
    System.out.println(collect);
  }

  public static void main2(String[] args) {
    List<Integer> list = Arrays.asList(1, 2, 3);
    List<Integer> list2 =
        list.stream()
            .map(x -> x * 10)    // 10, 20,  30
            .filter(x -> x > 10) //     20,  30
            .map(x -> x * 10)    //     200, 300
            .filter(x -> x < 300)//     200
            .map(x -> x * 10)    //     2000, 2000
            .map(x -> Stream.of(x - 1, x, x + 1))
            .flatMap(x -> x)
            .collect(Collectors.toList()); // 1999, 2000, 2001
    System.out.println(list);
    System.out.println(list2);
  }

}
