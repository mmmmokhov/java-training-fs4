package lesson14;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CountNumbers1 {

  public static Map<String, Long> countLetters(int min, int max) {
    return IntStream.rangeClosed(min, max)
        .mapToObj(String::valueOf)
        .collect(Collectors.joining())
        .codePoints()
        .mapToObj(c -> String.format("`%c`", c))
        .collect(Collectors.groupingBy(
            Function.identity(),
            Collectors.counting()
        ));
  }

  public static void main(String[] args) {
    Map<String, Long> characterIntegerMap = countLetters(173, 215);
    System.out.println(characterIntegerMap);

  }
}
