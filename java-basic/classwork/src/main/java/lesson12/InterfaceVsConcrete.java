package lesson12;

public class InterfaceVsConcrete {

    interface A {
        void m1();
    }

    static class A1 implements A {
        @Override
        public void m1() {}
        public void m2() {}
    }

    public static void main(String[] args) {
        A1 a1 = new A1();
        a1.m1();
        a1.m2();

        A a = new A1();
        a.m1();
//        a.m2();
    }
}
