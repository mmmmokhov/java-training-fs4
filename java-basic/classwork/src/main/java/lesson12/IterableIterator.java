package lesson12;

import java.util.*;

public class IterableIterator {

    public static void main(String[] args) {
        int[] a = {1, 2, 3};

        {
            int j = 0;
            while (j < a.length) {
                System.out.println(a[j]);
                j++;
            }
        }
//        System.out.println(j);


        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }

        for (int x : a) {
            System.out.println(x);
        }

        List<Integer> ints = Arrays.asList(1, 2, 3);

        for (int i = 0; i < ints.size(); i++) {
            System.out.println(ints.get(i));
        }

        for (int x : ints) {
            System.out.println(x);
        }

        HashSet<Integer> set = new HashSet<>(ints);

        for (int x : set) {
            System.out.println(x);
        }

        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, "a");
        map.put(2, "b");

        for (Map.Entry<Integer, String> x : map.entrySet()) {
            System.out.println(x);
        }

        {
            Iterator<Integer> it = ints.iterator();
            while (it.hasNext()) {
                int x = it.next();
                System.out.println(x);
            }
        }
        for (Iterator<Integer> it = ints.iterator(); it.hasNext(); ) {
            int x = it.next();
            System.out.println(x);
        }


    }

}
