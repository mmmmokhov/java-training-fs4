package lesson12;

import java.util.Iterator;

public class MonthApp {

    public static void main(String[] args) {
        Months months = new Months();

        for (String m: months) {
            System.out.println(m);
        }

        System.out.println("===========");

        for(Iterator<String> it = months.iterator(); it.hasNext();) {
            String m = it.next();
            System.out.println(m);
        }

        System.out.println("===========");
        Iterator<String> it = months.iterator();
        for(; it.hasNext();) {
            String m = it.next();
            System.out.println(m);
        }
        String m = it.next(); // java.lang.ArrayIndexOutOfBoundsException: Index 12 out of bounds for length 12
        System.out.println(m);

    }

}
