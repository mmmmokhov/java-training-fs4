package lesson12;

import java.util.Iterator;

public class Months implements Iterable<String> {

    private final String[] data = {
            "JANUARY",
            "FEBRUARY",
            "MARCH",
            "APRIL",
            "MAY",
            "JUNE",
            "JULY",
            "AUGUST",
            "SEPTEMBER",
            "OCTOBER",
            "NOVEMBER",
            "DECEMBER"
    };


    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {

            int index = 0;

            @Override
            public boolean hasNext() {
                return index < data.length;
            }

            @Override
            public String next() {
                String m = data[index];
                index++;
                return m;
            }
        };
    }

}
