package lesson06;

public class Owen {

    Pizza[] pizzas;

    public Owen(Pizza[] pizzas) {
        this.pizzas = pizzas;
    }

    public Owen() {
        this.pizzas = new Pizza[] {
                Pizza.of("Margarita", 60, new String[]{"pepper"}),
                Pizza.withoutAddons("Margarita", 45)
        };
    }
}
