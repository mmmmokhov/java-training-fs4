package lesson06;

import java.io.File;
import java.util.Arrays;

import static libs.EX.NI;

public class Pizza {

    /*final*/ String name;
    /*final*/ int size;
    /*final*/ String[] addons;

    private Pizza(String name, int size, String[] addons) {
        this.name = name;
        this.size = size;
        this.addons = addons;
    }

//    private Pizza(String name, int size) {
//        this(name, size, new String[]{});
//    }

    static Pizza of(String name, int size, String[] addons) {
        return new Pizza(name, size, addons);
    }

    static Pizza withoutAddons(String name, int size) {
//        String x  = this.name;
        return new Pizza(name, size, new String[]{});
    }

    static Pizza fromJSON(String json) {
        // TODO: parse JSON
        // new Pizza(json.a json.b, json.c)
        //
        throw NI;
    }

    static Pizza fromFile(File filename) {
        // TODO: read file
        // TODO: parse file
        // new Pizza(parsed.a parsed.b, parsed.c)
        //
        throw NI;
    }

    @Override
    public String toString() {
        String ingredients = (addons.length == 0) ? "has no extra ingredients" :
                String.format("has ingredients %s", Arrays.toString(addons));
        return String.format("It's a pizza `%s` of size %d, %s", name, size, ingredients);
    }

    void printMe() {
        System.out.println(this);
    }

    public static void main(String[] args) {
        Pizza.withoutAddons("M", 30);
    }
}
