package lesson06;

class Person {
    String name;
}

class Student extends Person {
    String[] classes;
}

class Topic {
    String name;
    Double complexity;
}

class Professor extends Person {
    Topic[] topic;
}

class Curriculum {
    String name;
    Topic[] topic;
}

public class Education {

}
