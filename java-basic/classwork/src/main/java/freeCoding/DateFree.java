package freeCoding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateFree {
    public static void main(String[] args) {
        Date date = new Date();
        SimpleDateFormat format1;
        SimpleDateFormat format2;
        Instant timestamp1 = Instant.now();
        long secondsFromEpoch = Instant.ofEpochSecond(0L).until(Instant.now(), ChronoUnit.SECONDS);

        format1 = new SimpleDateFormat("dd.MM.yy kk:mm z");
        format2 = new SimpleDateFormat("День dd Месяц MM Год yyyy Время hh:mm:ss a");
        System.out.println(format1.format(date));
        System.out.println(format2.format(date));
        System.out.println(timestamp1);
        System.out.println(secondsFromEpoch);

        Instant timestamp = Instant.now();
        LocalDateTime ldt = LocalDateTime.ofInstant(timestamp, ZoneId.systemDefault());
        System.out.printf("%s %d %d at %d:%d%n", ldt.getMonth(), ldt.getDayOfMonth(),
                ldt.getYear(), ldt.getHour(), ldt.getMinute());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String stringDateForParsing = "2019-12-21";

        System.out.printf("String %s parsed as: %n", stringDateForParsing);
        Date date2;

        try {
            date2 = format.parse(stringDateForParsing);
            System.out.println(date2);
        } catch (ParseException e) {
            System.out.printf("String  %s unparsed with current format %s%n",
                    stringDateForParsing, format.toPattern());
        }

        List<String> zones = new ArrayList<>(ZoneId.getAvailableZoneIds());
        zones.forEach(System.out::println);
    }
}
