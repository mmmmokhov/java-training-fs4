package freeCoding;

import java.util.Iterator;

public class IteratorTest {
    public interface Iterable<T> {
        Iterator<T> iterator();
    }

    public interface Iterator<E> {
        boolean hasNext();
        E next();
        void remove();
    }

    public interface Collection<E> extends java.lang.Iterable<E> {}
}
