package freeCoding;

import java.util.Stack;

public class StackBooks {
    public static void main(String[] args) {
        Stack stack = new Stack();
        System.out.println(stack.empty());
        stack.push("book1");
        stack.push("book2");
        stack.push("book3");
        stack.push("book4");
        stack.push("book5");
        stack.push("book6");
        System.out.println(stack);
        stack.pop();
        stack.peek();
        System.out.println(stack.search("book1"));
        System.out.println(stack);
    }
}
