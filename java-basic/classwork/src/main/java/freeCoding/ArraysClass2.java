package freeCoding;

import java.util.Arrays;

public class ArraysClass2 {
    public static void main(String[] args) {
        int[] numbers = {167, -2, 16, 99, 26, 92, 43, -234, 35, 80};

        for (int i = numbers.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int tmp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = tmp;

                }
            }
            System.out.printf("%d ", numbers[i]);
        }
        System.out.println();

        int[] numbersCopy = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            numbersCopy[i] = numbers[i];
            System.out.printf("%d ", numbersCopy[i]);
        }
        System.out.println();

        // Java Arrays .sort

        Arrays.sort(numbers);

        System.out.println(Arrays.toString(numbers));

        // Java Arrays .copyOf

        int[] numbersCopy2 = Arrays.copyOf(numbers, numbers.length);

        System.out.println(Arrays.toString(numbersCopy2));

        // Java Arrays .copyOfRange

        int[] numbersCopy3 = Arrays.copyOfRange(numbers, 2,6);

        System.out.println(Arrays.toString(numbersCopy3));

        // .equals DON'T USE!!!

        int[] mass = {1, 2, 3};
        int[] mass2 = {1, 2, 3};

        System.out.println(mass.equals(mass2));

        // Java Arrays .equals

        System.out.println(Arrays.equals(mass, mass2));

        // Java Arrays .deepEquals

        int[][] deepMass = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] deepMassCopy = Arrays.copyOf(deepMass, deepMass.length);

        System.out.println("Равны ли эти двумерные массивы между собой?");
        System.out.println(Arrays.deepEquals(deepMass, deepMassCopy));
        System.out.println(Arrays.deepToString(deepMassCopy));

    }
}
