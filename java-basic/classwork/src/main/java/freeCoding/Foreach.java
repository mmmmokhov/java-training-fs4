package freeCoding;

public class Foreach {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 4, 5};
        for (int i : array) {
            System.out.println(i);
        }
        int[] array2 = new int[] { 1, 2, 3, 4, 5 };
        for (int i=0; i<array2.length;i++){
            array2[i] = array[i] * 2;       // изменяем элементы
            System.out.println(array2[i]);
        }

        // Перебор многомерных массивов в цикле
        int[][] nums = new int[][]
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                };
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                System.out.printf("%d ", nums[i][j]);
            }
            System.out.println();
        }
    }
}
