package freeCoding;

class Animal {
    void animalInfo() {
        System.out.println("Information about an animal: ...");
    }
}

class Cat extends Animal {
    void catInfo() {
        System.out.println("Information about a cat: ...");
    }
}

public class ExtendsClass {

    public static void main(String args[]) {
        Cat cat = new Cat();
        cat.animalInfo();
        cat.catInfo();
    }

}
