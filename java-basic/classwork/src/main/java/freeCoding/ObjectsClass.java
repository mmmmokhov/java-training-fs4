package freeCoding;

public class ObjectsClass {
    public static void main(String[] args) {
        Person tom = new Person("Tom");
        Person tom2 = new Person("Tom");
        Person bob = new Person("Bob");
        System.out.println("To string: " + tom.toString());
        System.out.println("Hash code: " + tom.hashCode());
        System.out.println("Get class: " + tom.getClass());
        System.out.println("Equals: " + tom.equals(bob));
        System.out.println("Equals: " + tom.equals(tom2));

    }

    static class Person {
        private String name;

        public Person(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Person " + name;
        }

        public int hashCode() {
            return 10 * name.hashCode() + 20456;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Person)) return false;

            Person p = (Person)obj;
            return this.name.equals(p.name);
        }
    }
}
