package freeCoding;

class GenericsClass<T> {
    T ob;

    GenericsClass(T o) {
        ob = o;
    }
    T getObject() {
        return ob;
    }

    void showType() {
        System.out.println("Type T: " + ob.getClass().getName());
    }
}

class GenericsDemo {
    public static void main(String[] args) {

        GenericsClass<Integer> iObject;

        iObject = new GenericsClass<>(777);

        iObject.showType();

        int value = iObject.getObject();
        System.out.println(value + " - it is value");

        GenericsClass<String> strOb = new GenericsClass<>("some text");

        strOb.showType();

        String str = strOb.getObject();
        System.out.println(str + " - its value");
    }
}