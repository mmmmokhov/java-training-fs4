package freeCoding;

public class ArraysClass {
    public static void main(String[] args) {
        int nums[] = new int[4];
        int[] nums2 = new int[5];
        int[] nums3 = new int[] {1, 2, 3, 5};
        int[] nums4 = {1, 2, 3, 5};
        int[][] nums5 = {{0, 1, 2}, {3, 4, 5}};
        nums5[1][0] = 44;

        int length = nums4.length;

        System.out.println(length);
        System.out.println(nums4[nums4.length-1]);
        System.out.println(nums5[1][0]);
        System.out.printf("Str1: %d %d %d \n", nums5[0][0], nums5[0][1], nums5[0][2]);
        System.out.printf("Str2: %d %d %d \n", nums5[1][0], nums5[1][1], nums5[1][2]);

    }
}
