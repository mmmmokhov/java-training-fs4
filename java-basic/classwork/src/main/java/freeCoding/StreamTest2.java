package freeCoding;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest2 {
    public static void main(String[] args) {
        int firstElem = IntStream.range(23, 666)
                .findFirst()
                .getAsInt();
        System.out.println(firstElem);

        boolean result = Stream.of(1, 2, 3, 4, 5)
                .anyMatch(x -> x == 3);
        System.out.println(result);

//        NOT WORK
        int firstElem2 = IntStream.range(23, 666)
                .skip(1)
                .findFirst()
                .getAsInt();
        System.out.println(firstElem2);

        Stream<Integer> numbersStream = Stream.of(1, 2, 3, 4, 5, 6);
        Optional<Integer> result2 = numbersStream.reduce((x, y) -> x * y);
        System.out.println(result2.get()); // 720
    }
}
