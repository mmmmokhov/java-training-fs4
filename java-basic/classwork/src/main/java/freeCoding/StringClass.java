package freeCoding;

import java.util.Arrays;

public class StringClass {
    public static void main(String[] args) {
        String str1 = "Java";
        String str11 = "Java";
        String str2 = new String();
        String str3 = new String(new char[]{'h', 'e', 'l', 'l', 'o'});
        String str4 = new String(new char[]{'w', 'e', 'l', 'c', 'o', 'm', 'e'}, 3, 4);

        char[] helloArray = str3.toCharArray();
        Integer x = Integer.valueOf(123); // преобразование в строку
        String str5 = String.join("$", "Hello", "World");
        String str6 = "JavaScript";
        char[] str7 = new char[2];
        str11.getChars(0, 2, str7, 0);
        String str8 = "Script";
        String str9 = "    NO spaces   ";
        String str10 = "Some text to split string on substrings by spaces";

        System.out.println(str1); // Java
        System.out.println(str2); //
        System.out.println(str3); // hello
        System.out.println(str4); // come
        System.out.println(str1.length()); // 4
        System.out.println(".toCharArray: " + Arrays.toString(helloArray));
        System.out.println(".concat: " + str3.concat(str4)); // объединение строк
        System.out.println(".valueOf: " + x);
        System.out.println(".join: " + str5);
        System.out.println(".compareTo: " + str1.compareTo(str6));
        System.out.println(".charAt: " + str1.charAt(0));
        System.out.println(".getChars: " + Arrays.toString(str7));
        System.out.println(".equals: " + str1.equals(str6));
        System.out.println(".equalsIgnoreCase: " + str1.equalsIgnoreCase(str6));
        System.out.println(".regionMatches: " + str1.regionMatches(true, 0, str6, 0, 4));
        System.out.println(".indexOf: " + str6.indexOf(str8));
        System.out.println(".lastIndexOf: " + str6.lastIndexOf(str8));
        System.out.println(".startsWith: " + str6.startsWith(str8));
        System.out.println(".endsWith: " + str6.endsWith(str8));
        System.out.println(".replace: " + str1.replace("ava", "avelin"));
        System.out.println(".trim: " + str9.trim());
        System.out.println(".substring: " + str9.substring(7, 12));
        System.out.println(".toLowerCase: " + str6.toLowerCase());
        System.out.println(".toUpperCase: " + str6.toUpperCase());
        System.out.println(".split: " + Arrays.toString(str10.split(" ")));
    }
}
