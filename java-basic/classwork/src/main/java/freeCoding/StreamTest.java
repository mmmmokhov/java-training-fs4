package freeCoding;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest {
    public static void main(String[] args) {


        int[] filteredInts = IntStream.of(7, 12, 222, 1)
                .filter(x -> x < 10)
                .sorted()
                .toArray();
        System.out.println(Arrays.toString(filteredInts));

        int[] mappedInts = IntStream.of(12, 333, 1)
                .map(x -> x + 7)
                .toArray();
        System.out.println(Arrays.toString(mappedInts));

        int[] ints = IntStream.of(2, 5)
                .flatMap(x -> IntStream.range(0, x))
                .toArray();
        System.out.println(Arrays.toString(ints));

        IntStream.of(7, 444, 1)
                .filter(x -> x < 10)
                .forEach(System.out::println);

        List<Integer> list = Stream.of(1, 2, 3)
                .collect(Collectors.toList());
        System.out.println(list);

        String s = Stream.of(1, 2, 3)
                .map(String::valueOf)
                .collect(Collectors.joining("-", "<", ">"));
        System.out.println(s);
    }
}
