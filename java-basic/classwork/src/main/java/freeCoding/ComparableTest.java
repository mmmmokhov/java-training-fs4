package freeCoding;

import java.util.TreeSet;

public class ComparableTest {
    class Person implements Comparable<Person>{
        private String name;
        Person(String name) {
            this.name = name;
        }
        String getName() {return name;}

        public int compareTo(Person p) {
            return name.length()-p.getName().length();
        }

    }

}
