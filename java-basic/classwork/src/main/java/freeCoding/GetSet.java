package freeCoding;

public class GetSet {
    public static void main(String[] args) {
        class Cat {

            private String name;
            private String color;

            public String getName() {
                return name;
            }
            public void setName(String a) {
                name = a;
            }
            public String getColor() {
                return color;
            }
            public void setColor(String color) {
                this.color = color;
            }

        }
    }
}
