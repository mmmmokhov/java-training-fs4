package freeCoding;

import java.util.Set;
import java.util.TreeSet;

public class HundredSet {
    public static void main(String[] args) {
        Set set = new TreeSet<>();
        while (set.size() < 100) {
            Long random = Math.round(Math.random() * 1000);
            set.add(random);
        }
        System.out.println(set);
    }


}
