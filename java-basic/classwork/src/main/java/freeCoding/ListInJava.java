package freeCoding;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListInJava {


    public static void main(String[] args) {
        class Animal {
            String y = "some text";
        }

        class Cat extends Animal {
            String x = "bla bla bla";
        }

        ArrayList<String> myArrayList = new ArrayList<String>();
        LinkedList<String> myLinkedList = new LinkedList<String>();

        myArrayList.add("We add an element to ArrayList!");
        myLinkedList.add("We add an element to LinkedList!");

        Cat myCat = new Cat();
        myArrayList.add(myCat.toString());
//        myArrayList.remove(1);
        myArrayList.remove(myCat.toString());
        myArrayList.add("Hey!");
        myArrayList.add("This");
        myArrayList.add("is");
        myArrayList.add("my");
        myArrayList.add("first");
        myArrayList.add("list!");
        myArrayList.remove(0);

        for (int i = 0; i < myArrayList.size(); i++) {
            System.out.println(myArrayList.get(i));
        }

        System.out.println(myArrayList);
        System.out.println(myLinkedList);


    }
}
