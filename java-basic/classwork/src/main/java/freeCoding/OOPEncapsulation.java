package freeCoding;

public class OOPEncapsulation {
    private int bankAccountNumber;
    private int bankAccountBalance;

    public void showBalance() {
        // code to show Balance
    }

    public void putMoney(int a) {
        if (a < 0) {
            // show error
        } else {
            bankAccountBalance = bankAccountBalance + a;
        }
    }
}
