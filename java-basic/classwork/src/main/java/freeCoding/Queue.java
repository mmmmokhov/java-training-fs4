package freeCoding;

import java.util.PriorityQueue;

public class Queue {
    public static void main(String[] args) {
        PriorityQueue<Integer> myPriorityQueue = new PriorityQueue<Integer>();

        myPriorityQueue.add(1);
        myPriorityQueue.add(2);
        myPriorityQueue.add(3);
        myPriorityQueue.add(3);
        myPriorityQueue.add(4);
        myPriorityQueue.add(5);
        myPriorityQueue.add(6);
        for (int pq : myPriorityQueue) {
            System.out.println(pq);
        }

        myPriorityQueue.remove();
        System.out.println(myPriorityQueue);

        myPriorityQueue.poll();
        System.out.println(myPriorityQueue);

        myPriorityQueue.remove(4);
        myPriorityQueue.offer(22);
        System.out.println(myPriorityQueue);
        System.out.println(myPriorityQueue.peek());
    }
}
