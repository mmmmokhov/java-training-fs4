package lesson05;

import java.util.Arrays;
import java.util.Random;

public class Random2 {
    public static void main(String[] args) {
        int[] randoms = new Random()
                .ints(10, 50)
                .distinct()
                .limit(30)
                .sorted()
                .toArray();
        System.out.println(Arrays.toString(randoms));
    }
}
