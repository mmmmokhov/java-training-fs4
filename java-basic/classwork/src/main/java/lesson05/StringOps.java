package lesson05;

public class StringOps {

    public static void main(String[] args) {
        String s0 = "Hello, Java!";
        //           012345678
        String s1 = s0.toUpperCase();
        String s2 = s0.toLowerCase();
        String s3 = s0.substring(3, 8);// "lo, J"
        boolean isEqualTo = s0.equals("Hello, Java!");
        boolean isHelloAtStart = s0.startsWith("Hello");
        boolean b = s0.endsWith("!");
        s0.isEmpty();

    }

}
