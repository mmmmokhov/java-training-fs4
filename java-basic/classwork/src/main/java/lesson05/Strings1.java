package lesson05;

public class Strings1 {

    public static void main(String[] args) {
        char[] data = {65, 66, 67, 'e'}; // ASCII
        String s0 = new String(data);
        System.out.println(s0);

        String line1 = "Hello";
        String line2 = "world";
        String line0 = line1 + ", " + line2 + "!";
        String line = String.format("%s, %s!", line1, line2);

        String line0a = """
                "Hello"
                World""";

        System.out.println("--");
        System.out.println(line0a);
        System.out.println("--");

        String s = new StringBuilder()
                .append("sdfdsf")
                .append("sdfdsf")
                .append(line1)
                .toString();
    }

}
