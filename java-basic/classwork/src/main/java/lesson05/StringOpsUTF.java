package lesson05;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class StringOpsUTF {

    public static void main(String[] args) {
        String s0 = "алекс";
        byte[] bytes = s0.getBytes(StandardCharsets.UTF_8);
        System.out.println(bytes[0]);

        // -
        bytes[0] = (byte) Character.toUpperCase(bytes[0]);
        System.out.println(bytes.length); // 10
        String s2 = new String(bytes);
        System.out.println(s2);

        // +
        char[] chars = new char[s0.length()];
        s0.getChars(0, 5, chars, 0); // 5
        chars[0] = Character.toUpperCase(chars[0]);
        String s3 = new String(chars);
        System.out.println(s3);

        //
        char[] chars1 = s0.toCharArray();

        int[] ints = s0.chars().toArray(); // 5
        System.out.println(Arrays.toString(ints));

        for (int i = 0; i < s0.length(); i++) {
            char x = s0.charAt(i);
            System.out.print(x);
        }

        s0.contains("world"); // true
        String repeat = s0.repeat(5);
        System.out.println(repeat);
    }

}
