package hackerRank;

import java.util.List;

public class ApplesAndOranges {

    public static void countApplesAndOranges(int s, int t, int a, int b, List<Integer> apples, List<Integer> oranges) {
        // Write your code here
        int ac = 0;
        for (int distance: apples
              ) {
            int pos = a + distance;
            if (pos >= s && pos <= t) ac++;
        }

        int oc = 0;
        for (int distance: oranges
        ) {
            int pos = b + distance;
            if (pos >= s && pos <= t) oc++;
        }



        System.out.printf("%d\n%d\n", ac, oc);


    }
}
