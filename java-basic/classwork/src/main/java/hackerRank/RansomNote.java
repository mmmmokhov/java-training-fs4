package hackerRank;

import lesson12.Counter;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import static java.util.stream.Collectors.*;
import static libs.EX.NI;

class Result {

    /*
     * Complete the 'checkMagazine' function below.
     *
     * The function accepts following parameters:
     *  1. STRING_ARRAY magazine
     *  2. STRING_ARRAY note
     */

    public static Map<String, Long> toMap(List<String> words) {
        return words
                .stream()
                .collect(groupingBy(w -> w, counting()));
    }

    public static boolean checkMagazine0(List<String> magazine, List<String> note) {
        Map<String, Long> iHave = toMap(magazine);
        Map<String, Long> iNeed = toMap(note);

        return iNeed.keySet().stream().allMatch(w -> iHave.getOrDefault(w, 0L) >= iNeed.get(w));
    }
    public static void checkMagazine(List<String> magazine, List<String> note) {
        String r = checkMagazine0(magazine, note) ? "Yes" : "No";
        System.out.println(r);
    }

}

public class RansomNote {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int m = Integer.parseInt(firstMultipleInput[0]);

        int n = Integer.parseInt(firstMultipleInput[1]);

        List<String> magazine = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .collect(toList());

        List<String> note = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .collect(toList());

        Result.checkMagazine(magazine, note);

        bufferedReader.close();
    }
}

