package libs;

/**
 * System.out.print(...) - JUST FORBIDDEN
 * Scanner(System.in) - JUST FORBIDDEN
 */
public class Console {

    public static void print(String line) {
        System.out.println(line);
    }

    public static void main(String[] args) {
        print("whatever");
        print(String.format("%d + %d = %d\n", 2, 3, 2 + 3));

        ScannerConstrained sc = new ScannerConstrained();
        String line = sc.nextLine();
        System.out.println(line);
    }

}
