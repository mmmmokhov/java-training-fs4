package lesson02;

import java.io.InputStream;
import java.util.Scanner;

public class input2 {
    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);

        System.out.println("Enter the number: ");

        int i = scanner.nextInt();

        System.out.println("You entered " + i);
    }
}
