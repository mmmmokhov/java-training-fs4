package lesson02;

public class LearnRandom {

    public static int random(int min, int max) {
        int range = max - min + 1;
        double rand = Math.random() * range;
        int result = (int) (rand + min);
        return result;
    }
    public static void main(String[] args) {
        double r = Math.random();
        double r2 = r * 10;
        double r3 = r * 20 - 10;  // -10...10
        double r4 = r * 180 - 30; //  -30...150

        int i2 = (int) (r * 10);
        int i3 = (int) (r * 20) - 10;
    }
}
