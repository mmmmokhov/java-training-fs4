package lesson02;

import java.io.InputStream;
import java.util.Scanner;

public class Input {
    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);

        System.out.print("Enter your name: ");

        String line1 = scanner.nextLine();
        System.out.print("'");
        System.out.print(line1);
        System.out.print("'");
        System.out.println();
    }
}
