package lesson02;

import java.util.Scanner;

public class TypeCast {
    public static void main(String[] args) {
        int x = 32770;
        short y = (short) x;
        System.out.println(y);
        long z = x;
        System.out.println(z);

    }
}
