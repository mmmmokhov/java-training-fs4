package lesson02;

import java.io.InputStream;
import java.io.PrintStream;

public class Output {
    public static void main(String[] args) {
        System.out.println("Hello");
        PrintStream out = System.out;
        PrintStream err = System.err;
        out.println("Hello");
        err.println("Something went wrong");
    }
}
