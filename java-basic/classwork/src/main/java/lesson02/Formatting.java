package lesson02;

public class Formatting {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "Oleksandr";

        String message = s1 + ", " + s2 + "!";
        System.out.println(message);
        String message2 = String.format("%s, %s!", s1, s2);
        System.out.println(message2);

        double x = 1.23456789101112;
        System.out.println(x);
        String xs = String.format("%.3f", x);
        System.out.println(xs);
        System.out.printf("%.3f", x);

        System.out.printf("%1$s %1$s %1$s ", s2);
    }
}
