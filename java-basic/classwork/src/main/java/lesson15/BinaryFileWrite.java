package lesson15;

import java.io.*;

class Knowledge implements Serializable {
  int id;
  String data;

  public Knowledge(int id, String data) {
    this.id = id;
    this.data = data;
  }
}

public class BinaryFileWrite {

  public static void main0(String[] args) throws IOException {
    File file = new File("contents.bin");
    FileOutputStream fos = new FileOutputStream(file);
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeByte(35);
    oos.writeDouble(44.5);
    oos.writeObject(new Knowledge(22, "Java"));
    oos.close();
  }

  public static void main(String[] args) throws IOException, ClassNotFoundException {
    File file = new File("contents.bin");
    FileInputStream fis = new FileInputStream(file);
    ObjectInputStream ois = new ObjectInputStream(fis);
//    int i = ois.readInt();
    byte b = ois.readByte();
    double d = ois.readDouble();
    Knowledge o = (Knowledge) ois.readObject();

    System.out.println(b);
    System.out.println(d);
    System.out.println(o.id);
    System.out.println(o.data);
  }

}
