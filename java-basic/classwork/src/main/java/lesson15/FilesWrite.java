package lesson15;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FilesWrite {
    public static void main(String[] args) throws IOException {
        String line = "Hello, World!";
        String fileName = "message.txt";
        File file = new File(fileName);
        FileWriter fw= new FileWriter(file);
        fw.write(line);
        fw.close();

    }
}
