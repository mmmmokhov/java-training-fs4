package lesson15;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class MyReader implements AutoCloseable {

  @Override
  public void close() {
    System.out.println("Java is closing MyReader...");
  }

}

public class TextFileRead {

  public static void main0(String[] args) throws IOException {
    String fileName = "java-basic/classwork/src/main/java/lesson15/message.txt";
    File file = new File(fileName);
    FileReader fr = new FileReader(file);
    BufferedReader br = new BufferedReader(fr, 16384);

    String line;
    while ((line = br.readLine()) != null) {
      System.out.println(line);
    }

    br.close();
  }

  public static void main1(String[] args) throws IOException {
    String fileName = "java-basic/classwork/src/main/java/lesson15/message.txt";
    File file = new File(fileName);
    FileReader fr = new FileReader(file);
    BufferedReader br = new BufferedReader(fr, 16384);
    String line = br.readLine();
    System.out.println(line);

    br.close();
  }

  public static void main2(String[] args) throws IOException {
    String fileName = "java-basic/classwork/src/main/java/lesson15/message.txt";
    File file = new File(fileName);
    FileReader fr = new FileReader(file);
    BufferedReader br = new BufferedReader(fr, 16384);

    while (true) {
      String line = br.readLine();
      if (line == null) break; // END of file
      System.out.println(line);
    }

    br.close();
  }

  public static void main3(String[] args) throws IOException {
    String fileName = "java-basic/classwork/src/main/java/lesson15/message.txt";
    File file = new File(fileName);
    FileReader fr = new FileReader(file);


    try (BufferedReader br = new BufferedReader(fr, 16384)) {
      while (true) {
        String line = br.readLine();
        if (line == null) break;
        System.out.println(line);
      }
    }
  }

  public static void main4(String[] args) throws IOException {

    try (MyReader r = new MyReader()) {
      System.out.println("I'm good");
    }

  }

  public static void main(String[] args) throws IOException {
    String fileName = "java-basic/classwork/src/main/java/lesson15/message.txt";
    Path path = Paths.get(fileName);
    Files.lines(path)
        .filter(s -> !s.isBlank())
        .map(x -> x.toUpperCase())
        .forEach(s -> System.out.println(s));
  }


}
