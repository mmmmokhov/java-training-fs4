package lesson09;

public class ProceduralExample {

    static int a;
    static int b;
    static int c;

    static void add() {
        c = a + b;
    }

    public static void main(String[] args) {
        a = 1;
        b = 2;
        add();
        System.out.println(c);

        a = 5;
        b = 10;
        add();
        System.out.println(c);
    }


}
