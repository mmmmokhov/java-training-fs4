package lesson09;

public interface Array {

    int length();

    void add(int x);

    int get(int idx);

    default void removeLast() {
        removeAt(length());
    }

    void removeAt(int idx);

    void addAt(int x, int idx);

}
