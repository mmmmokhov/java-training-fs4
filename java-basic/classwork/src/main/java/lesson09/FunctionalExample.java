package lesson09;

public class FunctionalExample {

    static int add(int a, int b) {
        int c = a + b;
        return c;
    }

    public static void main(String[] args) {
        int c1 = add(1,2);
        System.out.println(c1);

        int c2 = add(10,5);
        System.out.println(c2);
    }


}
