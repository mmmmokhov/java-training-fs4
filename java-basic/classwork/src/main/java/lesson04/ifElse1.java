package lesson04;

import java.util.Scanner;

import static libs.StringUtils.isInt;

public class ifElse1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        String line = s.nextLine();
        if (isInt(line)) {
            System.out.println("Integer was entered");
        } else {
            System.out.println("non-Integer (String) was entered");
        }
        System.out.println("going further");
    }
}
