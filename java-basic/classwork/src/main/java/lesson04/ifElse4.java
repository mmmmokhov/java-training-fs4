package lesson04;

import java.util.Scanner;

import static libs.StringUtils.*;

public class ifElse4 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        int x = s.nextInt();
        String message = switch (x) {
            case 3 -> "three";
            case 10,
                 11 -> "ten or eleven";
            case 50 -> "fifty";
            default -> String.format("else: %d\n", x);
        };
        System.out.println(message);
    }
}
