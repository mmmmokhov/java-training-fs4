package lesson13;

import lesson10.Pizza;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PizzaTest {

    public static void main(String[] args) {
        Pizza p1 = new Pizza("Mar", 60);
        Pizza p2 = new Pizza("4s", 45);
        Pizza p3 = new Pizza("Peperoni", 30);

        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(p1);
        pizzas.add(p2);
        pizzas.add(p3);
        Collections.sort(pizzas);

        System.out.println(pizzas);

    }
}
