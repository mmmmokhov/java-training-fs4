package lesson13;

public class Pizza13 {
    public final String name;
    public final Integer size;

    public Pizza13(String name, Integer size) {
        this.name = name;
        this.size = size;
    }

    @Override
    public String toString() {
        return "Pizza13{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
