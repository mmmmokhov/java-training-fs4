package lesson03;

import libs.Console;

import static libs.Console.print;

public class OutputApp {

    public static void main(String[] args) {

        // no imports required
        Console.print("Hello");

        // import static libs.Output2.print; - required
        print("Hello");
    }

}
