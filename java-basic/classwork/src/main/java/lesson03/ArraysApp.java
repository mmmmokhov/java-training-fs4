package lesson03;

import libs.Console;
import libs.ScannerConstrained;

import java.util.Arrays;

public class ArraysApp {
    public static void main(String[] args) {

        int [] a = new int[10];
        var b = new double[10];

        System.out.println(a);
        System.out.println();

        String s1 = "0110";
        char[] chars1 = s1.toCharArray(); // ['0', '1', '1', '0']
        int[] ints = new int[chars1.length];
        for (int i = 0; i < chars1.length; i++) {
            char cc1 = chars1[0]; // '0' - 48, '1' - 49
            int cc2 = cc1 - 48;
            ints[i] = cc2;
        }

    }

    static int sum(int[] ints) {
        int sum = 0;
        for (int i = 0; i < ints.length; i++) {
            sum = sum +ints[i];
        }
        return sum;
    }

    static int sum7(int[] ints) {
        return Arrays.stream(ints).sum();
    }

    // throw exception
    static int max(int[] ints) {
        if (ints.length == 0) {
            throw new RuntimeException("given array is empty");
        }
        int max = ints[0];
        for (int i = 0; i < ints.length; i++) {
            if (ints[i] > max) max = ints[i];
        }
        return max;
    }

    public static void main1(String[] args) {
        int[] a = {1,2,30,5};
        int m1 = max(a);
        int[] b ={};
        int m2 = max(b);
    }
}
